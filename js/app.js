
// Initialize Foundation
$(document).foundation();

// Setup the datepickers
var fromDate = new Pikaday({
    field: document.getElementById('fromDate'),
    firstDay: 1,
    defaultDate: new Date(2016, 7, 3),
    minDate: new Date(2016, 7, 3),
    maxDate: new Date(2016, 7, 21),
    yearRange: [2016],
    bound: false,
    container: document.getElementById('fromCalendar')
});

var toDate = new Pikaday({
    field: document.getElementById('toDate'),
    firstDay: 1,
    defaultDate: new Date(2016, 7, 3),
    minDate: new Date(2016, 7, 3),
    maxDate: new Date(2016, 7, 21),
    yearRange: [2016],
    bound: false,
    container: document.getElementById('toCalendar')
});
